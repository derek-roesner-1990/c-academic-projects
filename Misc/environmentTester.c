#include <limits.h>
#include <float.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{
printf("Min value of INT: %d\nSize in Memory: %d\n\n", INT_MIN, sizeof(INT_MIN));
printf("Max value of INT: %d\nSize in Memory: %d\n\n", INT_MAX, sizeof(INT_MAX));
printf("Max value of UINT: %u\nSize in Memory: %d\n\n", UINT_MAX, sizeof(UINT_MAX));
/*INT_MIN & INT_MAX & UINT_MAX work with sizeof since they are integer values.
The rest needed to be casted so providing an actual value of the appropriate datatype works*/
char c = 'a';
printf("Min value of CHAR: %d\nSize in Memory: %d\n\n", CHAR_MIN,  sizeof(c));
printf("Max value of CHAR: %d\nSize in Memory: %d\n\n", CHAR_MAX,  sizeof(c));
printf("Max value of UCHAR: %d\nSize in Memory: %d\n\n", UCHAR_MAX, sizeof(c));
long l = 5000;
printf("Min value of LONG: %d\nSize in Memory: %d\n\n", LONG_MIN, sizeof(l));
printf("Max value of LONG: %d\nSize in Memory: %d\n\n", LONG_MAX, sizeof(l));
printf("Max value of ULONG: %u\nSize in Memory: %d\n\n", ULONG_MAX, sizeof(l));
short s = 4;
printf("Min value of SHORT: %d\nSize in Memory: %d\n\n", SHRT_MIN, sizeof(s));
printf("Max value of SHORT: %d\nSize in Memory: %d\n\n", SHRT_MAX, sizeof(s));
printf("Max value of USHORT: %d\nSize in Memory: %d\n\n", USHRT_MAX, sizeof(s));
printf("Number of bits needed for a char: %d\nSize in Memory: %d\n\n", CHAR_BIT, sizeof(c));
float f = 1.25;
printf("Min value of FLOAT: %f\nSize in Memory: %d\n\n", FLT_MIN, sizeof(f));
printf("Max value of FLOAT: %f\nSize in Memory: %d\n\n", FLT_MAX, sizeof(f));

}

