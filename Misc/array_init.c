/* PROGRAM:  02_array_init.c 
   AUTHOR:    
   DATE:     20/09/11 
   PURPOSE:  Initialize an array with random numbers
             
*/

/**************************************************************************/
/* Declare include files
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/**************************************************************************/
/* Some defines to use in the program
 **************************************************************************/
#define NUM_RANDOM( min, max ) ( min + (int)( (double)max * rand() / ( RAND_MAX + 1.0 ) ) )
#define SIZE 10

/**************************************************************************/
/* Function prototypes 
 **************************************************************************/
int init_array( int a[], int low, int high );
int print_array( int a[] );


int main( void ) {

	int n[ SIZE ];

	srand( time (NULL ) );
  
	init_array( n, 1, 100 ); 
   
	print_array( n );
	
	reverse_array( n );
	
	print_array( n );

   	return 0; 
}


/**************************************************************************/
/* Initialize array with random numbers
 **************************************************************************/
int init_array( int a[ ], int low, int high ) {
	
	int i;

   	/* initialize elements of array n to 0 */
	for ( i = 0; i < SIZE; i++ ) {
		a[ i ] = NUM_RANDOM( low, high ); 
	} 
	return 0;

} 

/**************************************************************************/
/* Some defines to use in the program
 **************************************************************************/
int print_array( int a[ ] ) {

	int i;

   	printf( "%s%13s\n", "Element", "Value" );
   	for ( i = 0; i < SIZE;  i++ ) {
      		printf( "%7d%13d\n", i, a[ i ] );
   	} 
	return 0;
}

int reverse_array(int a[ ]) {
	int t=0;
	int size = SIZE - 1;
	int i;
	
	for(i=0;i<SIZE/2;i++){
		t=a[size-i];
		a[size-i]=a[i];
		a[i]=t;
	}	

return 0;
}
