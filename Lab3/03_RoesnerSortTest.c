/*
 *  03_Roesner.c
	Bubble Sort and Insertion Sort built upon designs shown at http://www.cprogramming.com/tutorial/computersciencetheory/sortcomp.html
	Bubble Sort - http://www.cprogramming.com/tutorial/computersciencetheory/sorting1.html
	Selection Sort - http://www.cprogramming.com/tutorial/computersciencetheory/sorting2.html
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void InitArray( int a[], int s);
void PrintArray( int a[], int s);
int BubbleSort( int a[], int s);
int ModifiedBubbleSort( int a[], int s);
int SelectionSort( int a[], int s);
/*int InsertionSort( int a[], int s); Insertion sort is incomplete. */
#define SIZE 100000

int main()
{
	int a[SIZE];
	
	srand(time(NULL));
	InitArray(a,SIZE);
	/*PrintArray(a,SIZE);*/
	SelectionSort(a,SIZE);
	/*PrintArray(a,SIZE);*/
	
return(0);
}

void InitArray( int a[], int s){
	int i;
	int min = 1;
	int max = 100;
	
	for(i = 0; i < s; i++){
		a[i] =  ( (rand() % max) + min);
	}
}

void PrintArray(int a[], int s){
	int i;
	
	printf("\n");
	
	for( i = 0; i <  s; i++){
		printf("#%d: %d\n",i,a[i]);
	}
}


/*It has been shown that no key-comparison algorithm can perform better than O(n*log(n)).*/ 
int BubbleSort(int a[], int s){
	int i;
	int j;
	int t;
	
	for(i=0; i<s; i++){
		
		for( j=0; j<s-1; j++){	
			
			if(a[j]>a[j+1]){
			   t = a[j+1];
			   a[j+1] = a[j];
			   a[j] = t;
			}
		}
	}
return(0);
}
 

 /*Modified Bubble Sort built upon the design of bubble sort from cprogramming.com*/

int ModifiedBubbleSort(int a[], int s){
	int i;
	int j;
	int t;
	int sorted;
	
	for(i=0; i<s; i++){
		sorted = 1;
		for( j=0; j<s-1; j++){	
			if(a[j]>a[j+1]){
				sorted = 0;
				t = a[j+1];
				a[j+1] = a[j];
				a[j] = t;
			}
		}
		if(sorted == 1){  return(1); }
	}
	return(0);
}

int SelectionSort(int a[], int s){
	int i;
	int j;
	int t;
	int index_of_min;
	
	for(i=0; i<s; i++){
		index_of_min = i;
		
		for(j=i; j<s; j++){
			if(a[index_of_min]>a[j]){
				index_of_min = j;
			}
		}
		t = a[i];
		a[i] = a[index_of_min];
		a[index_of_min] = t;
	}
return(0);
 }

/*int InsertionSort( int a[], int s){
	
	for sorting the array the array 52314 First, 
		2 is inserted before 5, resulting in 25314 Then,
		3 is inserted between 2 and 5,
		resulting in 23514 Next,
		one is inserted at the start,
		12354 Finally, 
		4 is inserted between 3 and 5, 12345 
	
	int i;
	int j;
	int t;
	int sortedTo;
	int minIndex;
	
	for(i=0; i<s; i++){
		sortedTo = i;
		for(j=sortedTo; j<(s-1); j++){
			if(a[j] > a[j+1]){
				minIndex = a[j+1];
			}
		}
	}
return(0);
}*/


