------------
Bubble Sort
------------
Last login: Mon Dec 28 00:52:52 on console
derek-roesners-macbook-pro:~ derekroesner$ cd Desktop/C/Lab3
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_Roesner_SortTest.c -ansi -pedantic -Wall -o BubbleSortTest_100
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./BubbleSortTest_100

real	0m0.003s
user	0m0.001s
sys		0m0.002s
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_Roesner_SortTest.c -ansi -pedantic -Wall -o BubbleSortTest_1000
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./BubbleSortTest_1000

real	0m0.012s
user	0m0.010s
sys		0m0.002s
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_Roesner_SortTest.c -ansi -pedantic -Wall -o BubbleSortTest_10000
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./BubbleSortTest_10000

real	0m0.694s
user	0m0.680s
sys		0m0.003s
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_Roesner_SortTest.c -ansi -pedantic -Wall -o BubbleSortTest_100000
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./BubbleSortTest_100000

real	1m8.389s
user	1m7.311s
sys		0m0.154s
------------------------
Modified BubbleSort
------------------------
derek-roesners-macbook-pro:~ derekroesner$ cd Desktop/C/Lab3
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_Roesner_SortTest.c -ansi -pedantic -Wall -o modBubbleSortTest_100
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./modBubbleSortTest_100

real	0m0.003s
user	0m0.001s
sys		0m0.002s
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_Roesner_SortTest.c -ansi -pedantic -Wall -o modBubbleSortTest_1000
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./modBubbleSortTest_1000

real	0m0.011s
user	0m0.009s
sys		0m0.002s
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_Roesner_SortTest.c -ansi -pedantic -Wall -o modBubbleSortTest_100000
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./modBubbleSortTest_10000

real	0m0.677s
user	0m0.666s
sys		0m0.003s
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_Roesner_SortTest.c -ansi -pedantic -Wall -o modBubbleSortTest_100000
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./modBubbleSortTest_100000

real	1m7.278s
user	1m6.162s
sys		0m0.150s

----------------
Selection Sort
----------------
derek-roesners-macbook-pro:~ derekroesner$ cd Desktop/C/Lab3
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_RoesnerSortTest.c -ansi -pedantic -Wall -o InsertionSortTest_100
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./InsertionSortTest_100

real	0m0.002s
user	0m0.001s
sys		0m0.002s
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_RoesnerSortTest.c -ansi -pedantic -Wall -o InsertionSortTest_1000
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./InsertionSortTest_1000

real	0m0.005s
user	0m0.003s
sys		0m0.002s
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_RoesnerSortTest.c -ansi -pedantic -Wall -o InsertionSortTest_10000
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./InsertionSortTest_10000

real	0m0.210s
user	0m0.203s
sys		0m0.002s
derek-roesners-macbook-pro:Lab3 derekroesner$ gcc 03_RoesnerSortTest.c -ansi -pedantic -Wall -o InsertionSortTest_100000
derek-roesners-macbook-pro:Lab3 derekroesner$ time ./InsertionSortTest_100000

real	0m20.312s
user	0m20.012s
sys		0m0.037s





























