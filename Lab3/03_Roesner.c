/*
 *  03_Roesner.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void InitArray( int a[], int s);
void PrintArray( int a[], int s);
int SearchArray( int a[], int k, int s);
int SortArray( int a[], int s);

#define SIZE 100

int main()
{
	int a[SIZE];
	int k = 0;
	int result;
	
	srand(time(NULL));
	printf("Intializing array of %d numbers.\n", SIZE);
	InitArray(a,SIZE);
	puts("Array initialized.");
	printf("Which number do you think isn't in the array? ");
	scanf("%d",&k);
	result = SearchArray(a,k,SIZE);
	result == (0) ? printf("The number %d was found...\n",k) : printf("You were right %d wasn't in the array!\n",k);
	PrintArray(a,SIZE);
	SortArray(a,SIZE);
	PrintArray(a,SIZE);
return(0);
}

void InitArray( int a[], int s){
	int i;
	int min = 1;
	int max = 100;
	
	for(i = 0; i < s; i++){
		a[i] =  ( (rand() % max) + min);
	}
}

void PrintArray(int a[], int s){
	int i;
	
	printf("\n");
	
	for( i = 0; i <  s; i++){
		printf("#%d: %d\n",i,a[i]);
	}
}

int SearchArray(int a[], int k, int s){
	int i;
	
	for( i = 0; i < s; i++){
		if(a[i] == k){return(0);}
	}
return(-1);
}


/*A modified bubble sort build upon the BubbleSort algorithm from http://www.cprogramming.com/tutorial/computersciencetheory/sorting1.html*/
int SortArray(int a[], int s){
	int i;
	int j;
	int t;
	int sorted;
	
	for(i=0; i<s; i++){
		sorted = 1;
		for( j=0; j<s-1; j++){	
			if(a[j]>a[j+1]){
				sorted = 0;
				t = a[j+1];
				a[j+1] = a[j];
				a[j] = t;
			}
		}
		if(sorted == 1){  return(1); }
	}
	return(0);
}


