/*
 *  Lab6 - Pig Latin
 *  Derek Roesner
 *  Purpose - To make a Pig Latin copy of each word in a phrase
			  and then output each pig latin word to the terminal.
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int pigLatin(char* word,char* ordway);
int pigLatin2(char* word,char* ordway);

int main(int argc, char* argv){
	char *word;
	char *ordway;
	char phrase[] = "I am creating Pig Latin";
	char delim[] = " ";
	int i = 0;
	for(word=strtok(phrase,delim); word != NULL; word=strtok(NULL,delim)){
		printf("word[%d] %s\n",i,word);
		ordway = (char*) malloc(sizeof(char) * (strlen(word) + 2));
		pigLatin(word,ordway);
		printf("%s\n",ordway);
		i++;
	}
	

}

/*int pigLatin(char* word, char* ordway){
	
	int i;
	
	for(i=1; word[i] != '\0';i++){
		ordway[i-1] = word[i];
	}
/* i is decremented so it can be used as a 0 based index for tringsay 
	This needs to be done because i was offset by 1 in the loop above
	in order to skip the first letter of word.
	i-=1;
	ordway[i++] = word[0];
	ordway[i++] = 'a';
	ordway[i] = 'y';
return(0);
}*/

/*
 pigLatin2 works the same way as the function pigLatin() but uses a second
 variable j, that is used as the index for the pigLatin word being created instead
 of manipulating the value of i to access the desired portions for each "array".  
 */
int pigLatin(char* word, char* ordway){
	
	int i,j;
	
	for(i=1,j=0; word[i] != '\0';i++,j++){
		ordway[j] = word[i];
	}
	
	ordway[j++] = word[0];
	ordway[j++] ='a';
	ordway[j] = 'y';
return(0);
}