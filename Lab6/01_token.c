/* PROGRAM:  token.c 
   AUTHOR:    
   DATE:     31/10/12 
   TOPIC:    strings
   PURPOSE:  Example of how to use strtok
   NOTES:   
             
*/

/**************************************************************************/
/* Declare include files
 **************************************************************************/
#include <stdio.h>
#include <string.h>

/**************************************************************************/
/* Main Function 
 **************************************************************************/
int main( ) {
	
	/* 
	 * str contains the string to tokenize
	 * delim contains the delimiter -- in this case a space
	 * tok will point to the next word 
         */

	int count = 0;
	char str[] = "This in an example of tokenizing strings";
	char delim[] = " ";
	char *tok;

	/*
	 * The first call done with the str
	 * The next call done with NULL
	 */

	for( tok = strtok( str, delim ); tok != NULL; tok = strtok( NULL, delim ) ) {
		printf("Token[%d] = %s\n", count, tok );
		count++;
	}
	return count;
}
