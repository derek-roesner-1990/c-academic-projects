/*
 *  strTokExample.c
 *  
 *
 *  Created by Derek Roesner on 06/11/12.
 *  Copyright 2012 Sheridan Institute of Technology and Advanced Learning. All rights reserved.
 *
 */



#include <stdlib.h>
#include <stdio.h>
#include<string.h>

int main( ) {
	int count = 0;
	char str[] = "This in an example";
	char delim[] = " ";
	char *tok;

	for(tok = strtok(str,delim); tok != NULL; tok = strtok(NULL, delim)){ 
		printf("Token[%d] = %s\n", count, tok );
		reverseToken(tok);
		printf("Token[%d] = %s\n", count, tok );
		count++;
	}
return count;
}

int reverseToken(char *tok){
	char *reversed;
	int length = 0;
	int i,j;
	
	for(i=0; tok[i] != '\0';i++){
		printf("%c\n",tok[i]);
		length++;
		}
	
	reversed = (char*) malloc(sizeof(char) * length);
	
	for(i=length-1,j=0;j<length;j++,i--){
		reversed[j] = tok[i];
		
	}
	
	printf("%s\n",reversed);
return(1);	
}