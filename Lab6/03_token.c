/* PROGRAM:  02_token.c 
   AUTHOR:    
   DATE:     31/10/12 
   TOPIC:    strings
   PURPOSE:  Example of how to use strtok
   NOTES:    delimter and string need to be in quotation
             
*/

/**************************************************************************/
/* Declare include files
 **************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define ARG "delimiter string"

void display_usage( char *, char * );

/**************************************************************************/
/* Main Function 
 **************************************************************************/
int main(int argc, char *argv[] ) {
	
	/* 
	 * str contains the string to tokenize
	 * delim contains the delimiter -- in this case a space
	 * tok will point to the next word 
         */

	int count = 0;
	char str[80];
	char delim[5];
	char *tok;

	if ( argc != 3 ) {
		display_usage( argv[0], ARG );
		exit( EXIT_FAILURE );
	}

	strcpy(delim, argv[1] );
	strcpy(str, argv[2] );

	/*
	 * The first call done with the str
	 * The next call done with NULL
	 */

	for( tok = strtok( str, delim ); tok != NULL; tok = strtok( NULL, delim ) ) {
		printf("Token[%d] = %s\n", count, tok );
		count++;
	}
	return count;
}

/*********************************************************************
 * display_usuage
 ********************************************************************/
void display_usage( char * prog, char *opts ) {

	fprintf(stderr, "usage: %s %s\n", prog, opts );
	return;

}
