/*
 *  display_usage.h
 *  C. Ayala
 *  11F_CST8234
 *
 */

#include <stdio.h>

#define OPTIONS "[-d] char [-s]  char file ..."

void display_usage( char *, char * );

