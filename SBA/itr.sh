#!/bin/bash
#
# Evaluation Script for itr - 12F_CST8234
# Written by C. Ayala
#

#----------------------------------------------------------
# Course Information Variables
#----------------------------------------------------------
COURSE="CST8234 - Fall 2012"
TEACHER="CAROLINA AYALA"
SBA="itr"
FILE="_itr"

#----------------------------------------------------------
# Get Student Information
#----------------------------------------------------------
clear
echo -n "Lastname: "
read LASTNAME 
echo -n "Name: "
read NAME

#----------------------------------------------------------
# Create a unique FileName using LASTNAME+DATE
# Append info about student & course
#----------------------------------------------------------
OUTFILE="$LASTNAME$LABFILE.`date '+%b_%d'`" 
echo "$COURSE" > $OUTFILE
echo "$SBA" >> $OUTFILE
echo "Student Name:  $NAME $LASTNAME" >> $OUTFILE
echo "Date:  `date`" >> $OUTFILE
echo $COM$COM >> $OUTFILE
echo >> $OUTFILE

#----------------------------------------------------------
# LAB INFO
#----------------------------------------------------------
echo "$COM SBA: $SBA Marking$COM" >> $OUTFILE
echo "">> $OUTFILE



#----------------------------------------------------------
# Global Variables
#----------------------------------------------------------
COM="-------------------------"
GRADE=0
MAXGRADE=15

BASEDIR=`basename $PWD`
UTIL="tr"
PROG="./itr"
C_FILE="itr.c"
PROTOTYPE="void itr( const char *file, int tr, char display_usage.c itr.c )"
INVOP="o"
OPT="ds"

GREEN='\E[32m'
RED='\E[31m'

#----------------------------------------------------------
# Functions 
#----------------------------------------------------------
function Check {
	echo -e "\t$COM  [ $2 ]" >> $OUTFILE
	echo -e -n "\t$COM [$1 $2 "
	tput sgr0
	echo "]" 
	echo -e "\t\t$3"
}

function Header {
	echo -n "$1" >> $OUTFILE 
	echo -n "$1"  
}

echo $COM$COM$COM
echo "Evaluating $SBA out of $MAXGRADE"

#----------------------------------------------------------
# TEST00: Name Convenction
#----------------------------------------------------------
Header "Name Convenction      " 
echo $BASEDIR > 00.out
ls -l >> 00.test
`[ $BASEDIR == "$LASTNAME"_SBA ]` && `test -f $C_FILE`

if [ $? == 0 ]; then
	let GRADE+=1
	Check $GREEN OK
else
	Check $RED FAIL
fi

#----------------------------------------------------------
# TEST01: Compilation:  ansi 
#----------------------------------------------------------
Header "Ansi code        " 
`gcc -o itr itr.c display_usage.c -ansi -pedantic > 01.out`
`gcc -o itr itr.c display_usage.c -ansi -pedantic &> /dev/null`
 
if [ $? == 0 ]; then
	let GRADE+=1
	Check $GREEN OK
else
	Check $RED FAIL
fi

#----------------------------------------------------------
# TEST02: Function Prototype
#----------------------------------------------------------
Header "Function Prototype " 
`grep "void itr" $C_FILE > 02.out`
FUNC=`grep "void itr" $C_FILE`
FUNC=`echo $FUNC | cut -d";" -f1`
`[ "$FUNC" == "$PROTOTYPE" ]`
 
if [ $? == 0 ]; then
	let GRADE+=1
	Check $GREEN OK
else
	Check $RED FAIL
fi

#----------------------------------------------------------
# TEST03: Check Invalid options
#----------------------------------------------------------
Header "Testing invalid options"
$PROG -$INVOP &> 03.out
OUTPUT=$($PROG -$INVOP 2>&1)

if [ $? != 0 ]; then
	`echo $OUTPUT | grep usage &> /dev/null`
	if [ $? == 0 ]; then 
		let GRADE+=2
		Check $GREEN OK
	else
		Check $RED FAIL
	fi
fi

#----------------------------------------------------------
# TEST04: Check enough arguments 
#----------------------------------------------------------
Header "No enough arguments" 
`$PROG -d &> 04.out`
`$PROG -d &> /dev/null`
 
if [ $? != 0 ]; then
	let GRADE+=1
	Check $GREEN OK
else
	Check $RED FAIL
fi

#----------------------------------------------------------
# TEST05:  Non existing file 
#----------------------------------------------------------
Header "Non existing file" 
`$PROG -s NOFILE.txt &> 05.out`
`$PROG -s NOFILE.txt &> /dev/null`
 
if [ $? != 0 ]; then
	let GRADE+=1
	Check $GREEN OK
else
	Check $RED FAIL
fi

#----------------------------------------------------------
# TEST06: Option [-d] without char with 01_data.txt
#----------------------------------------------------------
Header "Option -d NO CHAR    " 
`$PROG -d  01_data.txt &> 06.out`
`$PROG -d  01_data.txt &> /dev/null`
 
if [ $? != 0 ]; then
	let GRADE+=1
	Check $GREEN OK
else
	Check $RED FAIL
fi

#----------------------------------------------------------
# TEST07: Use of perror
#----------------------------------------------------------
Header "Use of perror      " 
`grep perror $C_FILE > 07.out`
`grep perror $C_FILE &> /dev/null`
 
if [ $? == 0 ]; then
	let GRADE+=1
	Check $GREEN OK
else
	Check $RED FAIL
fi

#----------------------------------------------------------
# TEST07: Option [-d]  with 01_data.txt
#----------------------------------------------------------
Header "Option -d          " 
`$PROG -d t 01_data.txt > 08.out`
`$UTIL -d t < 01_data.txt > 08.test`
`diff -w 08.out 08.test &> /dev/null`
 
if [ $? == 0 ]; then
	let GRADE+=2
	Check $GREEN OK
else
	Check $RED FAIL
fi
#----------------------------------------------------------
# TEST09: Option [-s]  with 02_data.txt
#----------------------------------------------------------
Header "Option -s         " 
`$PROG -s i 02_data.txt > 09.out`
`$UTIL -s i < 02_data.txt > 09.test`
`diff -w 09.out 09.test &> /dev/null`
 
if [ $? == 0 ]; then
	let GRADE+=2
	Check $GREEN OK
else
	Check $RED FAIL
fi

#----------------------------------------------------------
# TEST06: Option [-s]  with 01_data.txt
#----------------------------------------------------------
Header "Option -s         " 
`$PROG -s i 01_data.txt > 10.out`
`$UTIL -s i < 01_data.txt > 10.test`
`diff -w 10.out 10.test &> /dev/null`
 
if [ $? == 0 ]; then
	let GRADE+=1
	Check $GREEN OK
else
	Check $RED FAIL
fi

#----------------------------------------------------------
# TEST11: Multiple files 
#----------------------------------------------------------
Header "Multiple files         " 
`$PROG -d i  01_data.txt 02_data.txt > 11.out`
`diff -w 11.out 11.test &> /dev/null`
 
if [ $? == 0 ]; then
	let GRADE+=1
	Check $GREEN OK
else
	Check $RED FAIL
fi

#----------------------------------------------------------
# User Final Information
#----------------------------------------------------------
echo
echo -e -n "$GREEN$NAME"
tput sgr0
echo -n ", your SBA $SBA Automatic Mark is "
echo -e "$RED$GRADE"
tput sgr0
echo $COM$COM$COM
echo -en "Waiting for $GREEN$TEACHER"
tput sgr0
echo " to verify your program"
echo "Please zip your directory with all your files and upload to BB"
echo
echo

#----------------------------------------------------------
# User Final Information into OUTFILE
#----------------------------------------------------------
echo >> $OUTFILE
echo "Automatic grade for $NAME $LASTNAME is :  "$GRADE >> $OUTFILE
more *.out >> $OUTFILE

#----------------------------------------------------------
# Clean up 
#----------------------------------------------------------
`rm -rf *.out &> /dev/null`
`rm -rf 08.test 10.test &> /dev/null`

