/*
 *  display_usage( )
 *  C. Ayala
 *  11F_CST8234
 *
 */
#include "display_usage.h"

/*********************************************************************
 * display_usuage
 ********************************************************************/
void display_usage( char * prog, char *opts ) {

	fprintf(stderr, "usage: %s %s\n", prog, opts );
	return;

}
