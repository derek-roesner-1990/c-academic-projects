/* PROGRAM:  itr.c 
   AUTHOR:   C. Ayala
   DATE:     08/12/11 
   TOPIC:    SBA 
   PURPOSE:  SBA 
   NOTES: Completed by Derek Roesner  
*/
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "display_usage.h"

#define BUFFERSIZE 1024

void itr( const char *file, int tr, char *c );

/**************************************************************************/
/* MAIN 
 **************************************************************************/
int main(int argc, char *argv[] ) {
	int flag;
	int sFlag = 0;
	int dFlag = 0;
	int type = 0; 
	char * c;
	
	while( (flag = getopt(argc,argv,":d:s:h")) != -1){
		
		switch(flag){
			case'd':
				dFlag = 1;
				c = optarg;
				break;
			case's':
				sFlag = 1;
				c = optarg;
				break;
			case 'h':
				puts("----------HELP----------");
				display_usage(argv[0],OPTIONS);
				break;
			case '?':
				fprintf(stderr,"Invalid Argument %c found\n",optopt);
				display_usage(argv[0],OPTIONS);
				exit(EXIT_FAILURE);
				break;
			case ':':
			    /* Can't actually get this to work 
				 For example ./itr -s -d  01_data.txt
				 will result in c being assigned -d*/
				printf("Argument not provided for %c flag",optopt);
				display_usage(argv[0],OPTIONS);
				exit(EXIT_FAILURE);
				break;
		}
	}
	
	/*printf("c %s\n",c);*/
	
	if(dFlag == 1){
		type = 1;
	}else if(sFlag == 1) {
		type = 2;
	}
	
	if(argc - optind < 1 || type == 0){
		puts("A file has not been provided");
		display_usage(argv[0],OPTIONS);
		exit(EXIT_FAILURE);
	}
	
	while(optind < argc){
		itr(argv[optind++],type,c);
	}

	return 0;
}


/**************************************************************************/
/* itr:
	const char * file:  filename of the file to be process
	int tr		 :  transformation type ( d or s )
	char *c		 :  char to be transformed
 **************************************************************************/
void itr( const char *file, int tr, char *c){
	int i;
	int fd;
	int bytes;
	char buffer[BUFFERSIZE];
	
	if( (fd = open(file,O_RDONLY)) == -1 ){
		perror("Source File");
		exit(EXIT_FAILURE);
	}
	
		while( (bytes = read(fd,buffer,BUFFERSIZE)) > 0){
			
			for(i = 0; i < bytes; i++){
				if(tr == 1){
					if(buffer[i] != *c){
						printf("%c",buffer[i]);
					}
				}
				
				if(tr == 2){
					if(buffer[i] == *c){
						printf("%c",buffer[i]);
						while(buffer[i] == buffer[i+1]){
							i++;	
						}
					}else{
					    printf("%c",buffer[i]);	   
					}
				}
			}
		}
	
	return;
}
