/************************************************************************
 * C Programming - CST8234
 * Lab Section - 400
 * Professor - Carolina Ayala
 * Lab #7 - Ciper Text 
 * Student - Derek Roesner 
 * Student# - 040668227 
*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define OPTIONS "[-d|e -h -?] key sourcefile destinationfile"
#define BUFSIZE 1024

void display_usage( char * prog, char *opts );

int main(int argc, char *argv[]){
	int key;
	int sFile;
	int dFile;
	int bytesRead;
	int bytesWrote;
	int i;
	int mode = 0;	
	char res;
	unsigned char line[BUFSIZE]; 

	while( (i=getopt(argc,argv,"deh?")) != -1){
		switch(i){
			case 'd':
				if(mode == 2){
						puts("Encryption has already been choosen, but the decryption flag was found.\n"      							     "       Do you want to change the cipher mode to decryption?");
					
					res = getc(stdin);

					if(res != 'y'){
						break;
					}				

				}
				mode = 1;
			break;
			case 'e':
				if(mode == 1){
						puts("Decryption has already been choosen, but the encryption flag was found.\n"      							     "        Do you want to change the cipher mode to encryption?");
					
					res = getc(stdin);

					if(res != 'y'){
						break;
					}
				}
				mode = 2;
			break;
			case 'h': case '?':
				puts("----------HELP----------");
				display_usage(argv[0],OPTIONS);
				exit(EXIT_FAILURE);
				
			break;		
		}
	} 
	
	/*checks to make sure all 3 needed arguments are there and that a cipher mode was choosen*/
	if (argc - optind != 3 || mode == 0) {
			display_usage( argv[0], OPTIONS);
			exit( EXIT_FAILURE );
		}	

	key = atoi(argv[optind++]);

	if( (sFile = open(argv[optind++] , O_RDONLY)) == -1 ){
		perror("sourcefile");
		exit(EXIT_FAILURE);
	}

	if( (dFile = open(argv[optind++] , O_WRONLY | O_CREAT, 0666)) == -1 ){
		perror("destfile");
		exit(EXIT_FAILURE);
	}
	

	while((bytesRead = read(sFile,line,BUFSIZE)) > 0){
	
		for(i = 0; i < bytesRead; i++){
			
			/*decryption*/ 
			if(mode == 1){
				line[i] -= key;
			} 			

			/*encryption*/
			if(mode == 2){	
				line[i] += key;
			}

			/* 
				These can also be used in backwards order to encrypt a file. 
			   For example:
			   Calling decrypt using a plain text file will cause it to be encrypted and then calling   					encrypt on the same file would cause it to be decrypted. 
				How to prevent this? use flags? detect invalid characters? 
			*/

		}
			
		if((bytesWrote = write(dFile,line,bytesRead)) < bytesRead){		
			puts("Error writing current line to file");
		return(1);
		}
	} 

	(mode == 1) ? puts("File successfully decrypted") : puts("File successfully encrypted");
	
	return(0);
}

void display_usage( char *prog, char *args ) {

	printf("Usage:%s %s\n", prog, args);
	return;
}

