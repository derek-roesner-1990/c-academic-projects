/* PROGRAM:  list.c 
   AUTHOR:    
   DATE:     20/10/11 
   TOPIC:    simple linked list implementation 
   PURPOSE:  12F_Midterm II - Solution
   NOTES:   
             
*/

/**************************************************************************/
/* Declare include files
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>

/**************************************************************************/
/* Struct Definitions 
 **************************************************************************/

struct node {
	int data;
	struct node *next;
};


/**************************************************************************/
/* Function prototypes 
 **************************************************************************/
void Push( struct node **, int );
void printList( struct node * );
void RemoveDuplicates( struct node * );
struct node* ShuffleMerge( struct node *, struct node * );


/**************************************************************************/
/* Function prototypes 
 **************************************************************************/
int main( void ) {

	struct node *a = NULL;
	struct node *b = NULL;
	struct node *c = NULL;

	Push( &a, 10 );
	Push( &a, 8 );
	Push( &a, 4 );
	Push( &a, 2 );

	Push( &b, 11 );
	Push( &b, 10 );
	Push( &b, 9 );
	Push( &b, 9 );
	Push( &b, 9 );
	Push( &b, 5 );
	Push( &b, 2 );

	printList( a );
	printList( b );
	
	RemoveDuplicates( b );
	
	printList( b );

	c = ShuffleMerge(  a, b );

	printList( c );
	
	RemoveDuplicates( c );
	printList( c );

   	return 0; 
}


/**************************************************************************/
/* 
 **************************************************************************/
void printList( struct node *head) {

	struct node *current = head;


	fprintf( stdout, "{ " );
	while( current != NULL ) {
		fprintf( stdout, "%d -> ", current->data );
		current = current->next;
	}
		
	fprintf( stdout, " NULL }\n" );

	return;

}

/**************************************************************************/
/* 
 **************************************************************************/
void Push( struct node **headRef, int data ) {

	struct node *new = malloc( sizeof( struct node ) );

	new->data = data;
	new->next = *headRef;
	*headRef   = new; 

	return;

}

/**************************************************************************
 
**************************************************************************/
void RemoveDuplicates( struct node * head){
	struct node *previous;
	int num;
	int atStart = 1; ;
	if(head == NULL){
		puts("List is empty");
		exit(EXIT_FAILURE);
	} else {
		while(head != NULL){
			if(atStart == 1){
				atStart = 0;
				num = head->data;
				head = head->next;
			}
			
			if(num == head->data){
				
				while(num == head->data){
					head = head->next;
				}
				
				previous->next = head;
				head = previous;
			}	
		num = head->data;
		previous = head;
		head = head->next;
		}
	}
}

void RemoveDuplicates2( struct node * head){
	struct node *previous;
	int num;
	if(head == NULL){
		puts("List is empty");
		exit(EXIT_FAILURE);
	} else {
		

		while(head != NULL){
			
			num = head->data;
			head = head->next;
			
			if(num == head->data){
				
				while(num == head->data){
					head = head->next;
				}
				
				previous->next = head;
				head = previous;
			}	
			//previous = head;
		}
	}
}



/**************************************************************************

 **************************************************************************/
struct node* ShuffleMerge( struct node *a, struct node *b ){
	struct node *c  = NULL; 
	struct node *head = NULL;
	int list = 1;
	
	while(a != NULL || b != NULL){
		
		if(list == 1){
			if(a != NULL){
				
				if( c == NULL){
					c = a;
					head = c;
				} else {
					c = a;
				}
				c = c->next;
				a = a->next;
			}
				list = 2;
		}
		
		
		if(list == 2){
			if( b != NULL){
				if(c == NULL){/*ends up being true once a == null since c points to a */
					c = b;
					head = c;
				} else {
					c = b;
				}
				c = c->next;
				b = b->next;
			}
				list = 1;
		}
			
	}
	c = head;
	return c;
}
