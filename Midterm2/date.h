struct date {
	int month;
	int day;
	int year;
};



int isLeapYear( struct date  );
int numberOfDays( struct date );
void Tomorrow( struct date , struct date * );
