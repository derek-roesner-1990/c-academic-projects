#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "date.c"
#define OPTIONS "DD MM YY"

void display_usage( char *, char * );

/*********************************************************************
 * Main Function
 ********************************************************************/
int main( int argc, char *argv[] ) {

	struct date today;
	struct date * tomorrow;
	char propArgs[] = "[int day XX][int month XX][int year XX]";
	
	if(argc != 4){ 
		display_usage(argv[0],propArgs);
		exit(EXIT_FAILURE);
	}else{
		today.day=atoi(argv[1]);
		today.month=atoi(argv[2]);
		today.year=atoi(argv[3]);
	}

	tomorrow = (struct date*) malloc(sizeof(struct date));
	Tomorrow( today, tomorrow );
	printf("Today is    %d/%d/%d\n", today.day, today.month, today.year);
	printf("Tomorrow is %d/%d/%d\n", tomorrow->day, tomorrow->month, tomorrow->year);

	printf("%lu - %lu\n", sizeof( today ), sizeof( tomorrow ) );
	
   	return 0; 
} 

/*********************************************************************
 * display_usuage
 ********************************************************************/
void display_usage( char * prog, char *opts ) {

	fprintf(stderr, "usage: %s %s\n", prog, opts );
	return;

}

