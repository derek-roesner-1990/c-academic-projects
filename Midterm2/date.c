#include "date.h"

int daysPerMonth[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
/**************************************************************************/
/* 
 **************************************************************************/
int isLeapYear( struct date d ) {

	if ( (d.year % 4 == 0 && d.year %100 ) || d.year % 400 )
		return 1;
	else
		return 0;
}
/**************************************************************************/
/* 
 **************************************************************************/
int numberOfDays( struct date d ) {

	int days;

	if (isLeapYear( d ) && d.month == 2 ) 
		days = 29;
	else
		days = daysPerMonth[d.month - 1 ];

	return days;

}
/**************************************************************************/
/*  TO BE IMPLEMENTED
 **************************************************************************/
void Tomorrow( struct date d , struct date * t) {
	d.day += 1;
	if(d.day > numberOfDays(d)){
		d.day = 1;
		d.month += 1;
		if(d.month > 12){
			d.month = 1;
			d.year += 1;
		}
	}

	t->day = d.day;
	t->month = d.month;
	t->year = d.year;
}
