#include <stdlib.h>
#include <stdio.h>
#include <time.h>


int rollDice(int min,int max);

const int MIN = 1;
const int MAX = 6;

int main()
{	char answer;
	int n = 0;
	int round = 1;
	int roll = 0;
	int point = 0;
	
	srand(time(NULL));
	
	/*The while loop below will keep the game going until return() is called within main().
	 If I wasn't returning specific values for a win or loss during the game, 
	 I could also exit the game by setting r to -1.
	*/
	while(n != -1){
		while(n == 0){
			printf("Press the r key to roll your dice: ");
			scanf("%c",&answer); 
				if( answer =='r' || answer =='R' ) {
					n = 1;
  					printf(" Rolling the dice...\n");
				} else {
					printf("You have pressed %c, an incorrect key.\n",answer);
  				}
  			scanf("%c",&answer);
  			/*need to take in the input in its entirety or read the input stream 
  			while( (scanf("%c",&answer) != char) ) <- checking for type in c? 
			reads the next character (likly '\n') from stdin and moves past it cause
			scanf("%c",&answer); leads to read of non rolls, if you type more than 1 character*/
		}
		while(n == 1){
			printf("Round #%d - ",round);
			roll = rollDice(MIN,MAX);
	
			switch(round){
			case 1:
				printf("\n---------------------------------\n"
					    "A roll of 7 or 11 wins.\n"
					    "A roll of 2, 3 or 12 loses.\n"
					    "A roll of 4 - 6 or 8 - 10 advances you to round 2. \n"
					    "----------------------------------\n");
				if(roll == 7 || roll == 11){
					printf("You rolled a %d. You win!\n", roll);
					return(0);
				}
				
				if(roll == 2 || roll == 3 || roll == 12){
					printf("You rolled a %d. You lose!\n", roll);
					printf("Press the y key to play again...:");
					scanf("%c",&answer);
				
					if(answer == 'y' || answer == 'Y'){
						round = 1;
						n=0;
						/*reads the next character (likely '\n') from stdin and moves past it*/
						scanf("%c",&answer);
					}else{ 
				 		printf("Thanks for playing!\n");				 	
				 		return(1); 
					}
				}
	
				if( (roll >= 4 && roll <= 6) || (roll >=8 && roll <= 10) ) {
					round++;
					point = roll;
					printf("You have advanced to round %d with the point %d.\n",round,point);
					n=0;
				}
			break;
			case 2:
				printf("Roll another %d to win!\n",point);
				
				if(roll == point){
					printf("Your roll of %d matches your point %d. YOU WIN!\n",roll,point);
					return(roll);
				}else if(roll == 7){
					printf("OH NO! You lost the game by rolling a 7.\n");
					return(roll);
				}else{
					printf("You rolled a %d, which neither wins or loses.\n",roll);
					n=0;
				}
			break;
			}
		}
	}
return(0);
}

/* (rand() % (max - min + 1) + min);  */

int rollDice(int min, int max){

int r1 = ((rand() % max) + min);
int r2 = ((rand() % max) + min);

return r1 + r2;
}

