/****************************************************************************************************************************
 * Student - Derek Roesner
 * Student# - 040668227
 * Assignment# - 1
 * C Programming - CST8234
 * Lab Section - 400  
 * Professor - Carolina Ayala  
 * Due - 12:00 Oct 15 2012 
 * Submission Made - Oct 15 2012
 * File(s) Included - sms.c
 *****************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define SIZE 1000

int init(int *memory, int *acc, int *instructCtr, int *instructReg , int *validInstructs, int *opCode, int *operand);
int loadInstruction(int *memory, int *memPointer, int *maxMemory, int *machineOn);
int executeInstruction(int *instructReg, int *memory, int *opCode, int *operand, int *instructCtr, int *acc, int *validInstructs,int* maxMemory, int *machineOn);
int memDump(int *memory, int *maxMemory);
int error(char* output);

int READ(int *memory, int *memLocation);
int WRITE(int *memory, int *memLocation);
int LOAD(int *memory, int *memLocation, int *acc);
int STORE(int *memory, int *memLocation, int *acc);
int ADD(int *memory, int *memLocation, int *acc);
int SUBTRACT(int *memory, int *memLocation, int *acc);
int DIVIDE(int *memory, int *memLocation, int *acc);
int MULTIPLY(int *memory, int *memLocation, int *acc);
int BRANCH(int *memLocation, int *instructCtr);
int BRANCHNEG(int *memLocation, int *instructCtr, int *accumulator);
int BRANCHZERO(int *memLocation, int *instructCtr, int *accumulator);
int HALT(int *machineOn);

int memory[SIZE];

int main(){
	
	/* 
	 * how to modify main so I can check if it was passed arguments?  
		main( char* args){
			if args[0] != ??;
		}
	*/
	
	int* opCode = (int*) malloc(sizeof(int));
	int* operand = (int*) malloc(sizeof(int)); 
	int* instructReg = (int*) malloc(sizeof(int));
	int* instructCtr = (int*) malloc(sizeof(int)); 	
	int* accumulator = (int*) malloc(sizeof(int)); 
	int* validInstructs = (int*) malloc(sizeof(int));
	int* maxMemory = (int*) malloc(sizeof(int)); 
	int* memPointer = (int*) malloc(sizeof(int)); 
	int* machineOn = (int*) malloc(sizeof(int));
	
	int errorThrown;
	int executeTo;
	int i;
	
	if(init(memory,accumulator,instructCtr,instructReg,validInstructs,opCode,operand) == 0){
		*machineOn = 1;
	}
	
	/*
	 * Could have used the code similar to what is below if I desgined loadInstruction() to load all instructions 
	 * in one call and executeInstruction() to also execute all instructions in one call. 
	 
	 * while(*machineOn == 1){
	 * loadInstructions();
	 * executeInstructions();
	 * }
	 * Should have used this design since it's easier to stop execution once a error is called in main().
	 */
	
	while(*machineOn == 1){
		
		while(errorThrown == 0){ errorThrown = loadInstruction(memory, memPointer, maxMemory, machineOn); }
		
		//prevents execution of the instructions if an error occured while loading the instructions. 
		if(errorThrown == 1) { break; }
		
		executeTo = *maxMemory - 1; 
		for(i=0; i<= executeTo; i++){
			errorThrown = executeInstruction(instructReg, memory, opCode, operand, instructCtr, accumulator, validInstructs, maxMemory, machineOn);
			if(errorThrown == 1){ break; }
		}
	}
	printf("\nREGISTERS:\n"
		   "accumulator:            %05d\n"
		   "instructionCounter:     %03d\n"
		   "instructionRegister:    %05d\n"
		   "operationCode:          %02d\n"
		   "operand:                %03d\n"
		   "validInstructions:      %d"
		   "\n\n"
		   ,*accumulator,*instructCtr,*instructReg,*opCode,*operand,*validInstructs);
	
	memDump(memory, maxMemory);
	}	
	
int init(int *memory, int *acc, int *instructCtr, int *instructReg , int *validInstructs, int *opCode, int *operand){
	
	int i;
	
	for(i = 0; i < SIZE; i++){ 
		memory[i] = 50505;
	}
	
	acc = 0;
	instructCtr = 0;
	instructReg = 0;
	validInstructs = 0;
	opCode = 0;
	operand = 0;
	
return (0);
}

int memDump(int *memory, int *maxMemory){
	int i;
	
	printf("MEMORY:");
	for(i = 0; i <= *maxMemory; i++, memory++){
		if(i % 10 == 0){
			printf("\n");
		}
		printf("%d:%d ", i, *memory);	
		}
	printf("\n");
	
return(0);
}

int loadInstruction(int *memory, int *memPointer, int *maxMemory, int *machineOn){
	
	if( fscanf(stdin,"%d", memPointer) ==  1){ 
		
		//printf("i:%d memPointer:%d\n",*maxMemory,*memPointer);

		if(*maxMemory == SIZE){
			error("*** ABEND: prg load: prg too big ***");
			HALT(machineOn);
			*maxMemory = SIZE - 1;	
			return(1);
		}
		
	   /*
		* The value of -1 is returned because the value -1 will exit the loop loading 
		* the instructions in main() without indicating that an error occuured. 
		*/
		if(*memPointer == -999999){ return(-1); }
		
	   if(*memPointer < -99999 || *memPointer > 99999){ 
		   error("*** ABEND: illegal input ***");
		   HALT(machineOn);
		   return(1); 
	   }
		
		memory[*maxMemory] = *memPointer;
	/*If memDump() is called after loadInstructions() has successfully completed execution
	* the memory location one space beyond what has been loaded with instructions will be printed.
	* 
	* The reason maxMemory's value is incremented though, is to check if it's
	* value is equivalent to SIZE on upcoming calls to loadInstructions(),
	* in order to prevent loading instructions into unallocated memory.
	*
	* Since memDump() will not be called after a successful call to loadInstructions()
	* but only if loadInstruct() encounters an error during execution. This issue won't
	* actually cause problems with the code's exection, but is noted anyways. 
	*/
		*maxMemory += 1;
	    }
return(0);
}
	   
int executeInstruction(int *instructReg, int *memory, int *opCode, int *operand, int *instructCtr, int *acc, int *validInstructs,int* maxMemory, int *machineOn){
	
		if( memory[*instructCtr]< -99999 || memory[*instructCtr] > 99999){
			//Really detecting a potential error
			error("*** ABEND: Addressability error ***"); 
			HALT(machineOn); 
		return(1);
	   }
	   
	   *instructReg = memory[*instructCtr];
	   *opCode = *instructReg / 1000;
	   *operand = *instructReg % 1000;
	printf("operand: %d", *operand); 
		if(*operand > 99){
			error("*** ABEND: prg load: prg too big ***");
			HALT(machineOn); 
		return(1);
		}

	   switch(*opCode){
		   case 10:
			   if( READ(memory,operand) == 1){ HALT(machineOn); return(1); }
			   *instructCtr += 1;
			break;
		   case 11:
			   WRITE(memory,operand);
			   *instructCtr += 1;
			break;
		   case 20:
			   LOAD(memory,operand,acc);
			   *instructCtr += 1;
			break;
		   case 21:
			   STORE(memory,operand,acc);
			   *instructCtr += 1;
			break;
		   case 30:
			   if(ADD(memory,operand,acc) == 1){ HALT(machineOn); return(1); }
			   *instructCtr += 1;
			break;
		   case 31:
			   if(SUBTRACT(memory,operand,acc) == 1){ HALT(machineOn); return(1); } 
			   *instructCtr += 1;
			break;
		   case 32:
			   if(DIVIDE(memory,operand,acc) == 1){ HALT(machineOn); return(1); }
			   *instructCtr += 1;
			break;
		   case 33:
			   if(MULTIPLY(memory,operand,acc) == 1){ HALT(machineOn); return(1); }
			   *instructCtr += 1;
			break;
		   case 40:
			   BRANCH(operand,instructCtr);
			break;
		   case 41:
			   BRANCHNEG(operand,instructCtr, acc);
			break;
		   case 42:
			   BRANCHZERO(operand,instructCtr, acc);
			break;
		   case 43:
			   HALT(machineOn);
			   return(1);
			break;
		   default:
			   error("*** INVALID OPCODE ***");
				HALT(machineOn);
			   return(1);
			break;
		}
	if(*maxMemory < *operand){*maxMemory = *operand;}
	*validInstructs += 1;
return(0);	   
}

int error(char* output){
	printf("%s\n",output);
return(0);
}

int READ(int *mem, int* memLocation){
	
	int word; 
	fscanf(stdin,"%d",&word);
	
	if( word > 99999 || word < -99999){ 
		error("*** ABEND: illegal input ***");
		return(1);
	}
	
	mem[*memLocation] = word;
	printf("READ: %05d\n", word);
return(0);
}

int WRITE(int *mem, int* memLocation){
/* Additional testing is not requied since the word used to create 
 the value in the pointer memLocation has been previously tested */
	printf("%05d\n", mem[*memLocation]);

return(0);
}
	   
		
int LOAD(int *mem, int *memLocation, int *acc){
/* Additional testing is not requied since the word used to create 
the value in the pointer memLocation has been previously tested */
	*acc = mem[*memLocation]; 
return(0);
}

int STORE(int *mem, int *memLocation, int *acc){
/* Additional testing is not requied since the word used to create 
the value in the pointer memLocation has been previously tested */	
	mem[*memLocation] = *acc;
return(0);
}


int ADD(int *mem, int *memLocation, int *acc){
    
	if( *acc + mem[*memLocation] > 99999 ) { // 5 + 99999
		error("*** ABEND: overflow ***");
		return(1);
	}
	
	if( *acc + mem[*memLocation] < -99999 ) { // -5 + -99999
		error("*** ABEND: underflow ***");
		return(1);
	}
	
	*acc += mem[*memLocation];
return(0);	
}
	   
int SUBTRACT(int *mem, int *memLocation, int *acc){
	
	if(*acc - mem[*memLocation] > 99999){ // 1 - (-99999)
		error("*** ABEND: overflow ***");
		return(1);
	}
	
	if( *acc - mem[*memLocation] < -99999 ){  //(-999999) - 1
		error("*** ABEND: underflow ***");
		return(1);
	}
	
	*acc -= mem[*memLocation];
return(0);
}
	   
int DIVIDE(int *mem, int *memLocation, int *acc){   
	
	if(mem[*memLocation] == 0){
		   error("*** ABEND: attempted division by 0 ***");
		   return(1);
	   }
	
	 *acc /= mem[*memLocation]; 
return(0);	   
}
	   
int MULTIPLY(int *mem, int *memLocation, int *acc){
	
	if(*acc * mem[*memLocation] > 99999){
		error("*** ABEND: overflow ***"); //2 * 50000 or  -2 * (-50000)
		return(1);
	}
	
	if( *acc * mem[*memLocation] < -99999){  //-2 * 50000
		error("*** ABEND: underflow ***");
		return(1);
	}
	
	*acc *= mem[*memLocation];
return(0);
}
	   
int BRANCH(int *memLocation, int *instructCtr){
	*instructCtr = *memLocation; 
return(0);
}

int BRANCHNEG(int *memLocation, int *instructCtr, int *accumulator){
	
	if(*accumulator < 0) { 
		*instructCtr = *memLocation; 
	} else if(*accumulator >= 0){
	/*
	* Allows execution to continue if the value in the accumulator is == or > 0.
	* This fails when tested using 18_BRANCHNEG_NORMAL.txt but only because 
	* the instructions read after the attempted branch access memory that has been modified.
	* Use the test file 22_BRANEG_NOTNEG.txt instead to achieve a successful execution. 
	*/
		*instructCtr += 1;
	}
	
return(0);
}
	   
int BRANCHZERO(int *memLocation, int *instructCtr, int *accumulator){
	if(*accumulator == 0) {
		*instructCtr = *memLocation; 
	} else if(*accumulator < 0 || *accumulator > 0){
	/*
	* Allows execution to continue if the value in the accumulator is != 0.
	* This fails when tested using 20_BRANCHNEG_NORMAL.txt but only because 
	* the instructions read after the attempted branch access memory that has been modified.
	* Use the test file 23_BRANEG_NONZERO.txt instead to achieve a successful execution. 
	*/
		*instructCtr += 1;
	}
return(0);
}

int HALT(int *machineOn){
	*machineOn = 0; 
	puts("*** Execution Terminated ***");
return(0);
}

	   

		

