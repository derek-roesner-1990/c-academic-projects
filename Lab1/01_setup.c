/* PROGRAM:  01_setup.c 
   AUTHOR:   Carolina Ayala 
   DATE:     11/09/11 
   PURPOSE:  Simple template to be used in CST8234 labs 
   NOTES:     
             
*/
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/**************************************************************************/
/* main:  
 ***************************************************************************/
int main( int argc, char ** argv ) {

	char c;

	/* 
 	 * If no argument is given, print usage and exit
	 * argv[0] contains the name of the executable
	 * EXIT_FAILURE is defined in stdlib as 1
	 */
	if (argc != 2 ) {
		printf( "Usage:  %s [ integer ]\n", argv[0] );
		exit ( EXIT_FAILURE );
	}

	/*
	 * Prints the size of a char
	 * sizeof returns a unsigned long integer
	 */
	printf("Sizeof ( char ):  %lu\n", sizeof(char) );

	/*
	 * Prints MAX and MIN values of a char 
	 * CHAR_MAX & CHAR_MIN defined in limits.h 
	 */
	printf("Max number of an char: %d\n", CHAR_MAX );
	printf("Min number of an char: %d\n", CHAR_MIN );

	/*
	 * Prints the integer value of the char entered
	 * c contains the first character in argv[1]
	 */

	c = argv[1][0];
	printf("Char %c is %d\n", c, c );

	return ( 0 );

}
