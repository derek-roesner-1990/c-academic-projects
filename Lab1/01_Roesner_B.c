/*
PROGRAM:  01_Roesner_B.c 
   AUTHOR:   Derek Roesner
   DATE:     05/09/12 
   PURPOSE:  Solution to Problem #2, Part #3
   NOTES: In order to make the test accurate only unsigned integers can be used.
*/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/*
----------------------------------------------------------
main;
----------------------------------------------------------
*/

int main(char ** argv) {

 unsigned int a;
 unsigned int b;
 
 printf("This program requires you to input two numbers in order to perform addition.\n");
 
  printf("Keeping in mind that the maximum value of a unsigned integer is: %u\n\n", UINT_MAX);
 
 printf("Enter first integer.\n");
 scanf("%u",&a);
 
 printf("Enter second integer.\n");
 scanf("%u",&b);
 

/*
  The approach below doesn't work because when an overflow occurs, truncation occurs
  and because of this the sum returned from (a+b) will not be greater than UINT_MAX
  but will be 0 or greater depending on the amount that the sum exceeds UINT_MAX by. 
 
  For example ( UINT_MAX + 2 = 1 )

 */
 if( a + b > UINT_MAX){
 	printf("The addition of the two requested values result in a truncated result\n");
 	printf("The addition will not be performed\n");
 	exit(1);
 }

 
 
 /*A solution taken from http://c-faq.com/misc/intovf.html to catch cases of integer overflow.
   
   However this approach doesn't work if a negative integer is assigned to b.
   For example: a = 1; b = -1;  (INT_MAX - (b) = 2147483648)
   And since 2147483648 isn't a assignable signed integer value, an overflow occurs
   making the returned sum actually -2147483648, and the if statement return true
   even though a+b = 0 and does not cause an overflow or truncation. 
   
   **In order to make the test accurate only unsigned integers can be used.**
*/
 
 if(UINT_MAX - b < a){
 	printf("The addition of the two requested values result in a truncated result\n");
 	printf("The truncated value is %u\n",a + b);
 	exit(1);
 }
 
 a+=b;
 printf("The sum is: %u\n",a);
 
return 0;

}