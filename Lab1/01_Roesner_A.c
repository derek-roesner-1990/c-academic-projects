/*
PROGRAM:  01_Roesner_A.c 
   AUTHOR:   Derek Roesner
   DATE:     05/09/12 
   PURPOSE:  Solution to Lab #1, Problem #1.
   NOTES: 1. Find out how many bytes are used to store a signed integer in your system. See sizeof
		  2. Find the max and min values that an integer can hold. See limits.h
          3. Read a signed integer number. See scanf( )
		  4. Finds the absolute value of the number. See abs()

*/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/*
----------------------------------------------------------
main;
----------------------------------------------------------
*/

int main(char ** argv) {

 int n;

	/*Prints the maximum and minimum value of a integer 
	on my system using the appropriate constants within limits.h*/
	
	printf("Max value a signed integer can hold: %d\n", INT_MAX );
	printf("Min value a signed integer can hold: %d\n", INT_MIN );
	
	
	/* scanf() is used to retrieve a integer from the console 
	   and assign it to variable n using the address operator */
	
	printf("Enter a signed integer: ");
	scanf("%d", &n);
	printf("The signed integer entered was %d\n", n);
	
	/*Prints the size of the signed integer in memory*/
	
	printf( "The size of a signed integer in memory is %lu bytes\n", sizeof(n) );
	
	
	/*Prints the absolute value of the integer n, absolute values are always positive*/
	
	printf("The absolute value of the signed integer %d is: %d\n", n, abs(n) ); 

return 0;

}