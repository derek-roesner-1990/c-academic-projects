/* PROGRAM:  circle.c 
   AUTHOR:   Carolina Ayala 
   DATE:     
   PURPOSE:  
   NOTES:   
             
*/

/**************************************************************************/
/* Declare include files 
 **************************************************************************/
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

/**************************************************************************/
/* Defines
 ***************************************************************************/
#define MAX_POINTS      72 

/**************************************************************************/
/* Declare function prototypes
 ***************************************************************************/
void Circle( float, float, float);

/**************************************************************************/
/* main: all initialization and callback registration.
 ***************************************************************************/
int main( void ) {

	Circle( 5.0, 0.0, 0.0 );

   	return( 0 );  /* NOTE: this is here only for ANSI requirements */
}

/**************************************************************************/
/* A function calculates the coordinates of a circle
 * with radius r centre in ( a, b )
 * with MAX_POINTS points
 **************************************************************************/
void Circle( float r, float a, float b) {



}
		

