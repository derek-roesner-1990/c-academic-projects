#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#define HELP "When using the program iCount, you are asked to provide one or more file(s) to read\n"\
"and zero or more flags that indicate what to look for within each file.\n"\
"The flag options are:\n"\
"-c Count all characters including spaces.\n"\
"-w Count the amount of words.\n"\
"-l Count the amount of newline characters.\n"\
"-p [pattern] Count of occurances within the file for the pattern provided.\n"\
"-i if given with -p, ignores case distinctions.      \n"\
"-v if given with -p, selects words that do not match the pattern provided.\n"\
"If no flags are provided by the user, the program will count\n"\
"the amount of characters, newline characters and words within each file.\n"\
"Usage: ./iCount [OPTIONS -c &| -w &| -l &| -p [pattern] &| -i &| v &| h ] [FILE]...\n" 

void count(char *fileArg,int *cFlag, int *lFlag, int *wFlag, int *pFlag, int *iFlag, int *vFlag);
void toLowerCase(char *word);
int getLineLength(char * toRead,char delimiter,int offset);
int comparePattern(char *word, char *pattern);
void results(char *resultsFor,int total);
void displayUsage(char *helpMessage);

char * pattern;
int flagFound = 0;
int cFlag = 0;
int charAmt = 0;
int cTotal;
int wFlag = 0;
int wordAmt = 0;
int wTotal;
int lFlag = 0;
int newLineAmt = 0;
int nTotal;
int pFlag = 0;
int pAmt = 0;
int pTotal;
int iFlag = 0;
int vFlag = 0;





