/****************************************************************************************************************************
 * Student - Derek Roesner
 * Student# - 040668227
 * Assignment# - 3
 * C Programming - CST8234
 * Lab Section - 400  
 * Professor - Carolina Ayala  
 * Due - 12:00 Dec 2 2012 
 * Submission Made - Dec 2 2012
 * File(s) Included - iCount.c
 *****************************************************************************************************************************/

#include <unistd.h>
#include "iCount.c"
/*stdlib.h, stdio.h and string.h are included in iCount.h which is included by iCount.c*/

int main(int argc, char * argv[]){
	int flag;
	int fileAmt = 0;
	int opterr = 0;
	extern char * optarg;
	extern int optopt, optind;
	
	printf("\n");
	
	while( (flag = getopt(argc,argv,":cwlhp:iv")) != -1){
			/*printf("%c\n",flag);*/
		flagFound  = 1;
		switch(flag){
			case 'c':
				cFlag = 1;
			break;
			case 'w':
				wFlag = 1;
		    break;
		    case 'l':
				lFlag = 1;
			break;
			case 'h':
				puts("----------HELP---------- ");
				displayUsage(HELP);
			break;
			case 'p': 
				pFlag = 1;
				pattern = optarg;
				/*printf("pattern:%s\n",pattern);*/
			break;
			case 'i':
			   iFlag = 1;
			break;
			case 'v':
			   vFlag = 1;
			break;
			case '?':
				fprintf(stderr,"Unrecognized option %c\n",optopt);
				opterr = 1;
			break;
			case ':':
				/*This does not seem to work. 
				 For example ./iCount -p -c text.txt 
				 will end up reading -c as the pattern
				 */
				fprintf(stderr,"%c flag requires an argument to use as a pattern.",optopt);
				opterr = 1;
			break;
		}
	
		if(opterr == 1){
			displayUsage(HELP);
		}
		
	}
	
	if(argc - optind < 1){
		puts("A file has not been provided, reading cannot be performed.\n");
		displayUsage(HELP);
	}
	
	if(flagFound == 0){
		puts("No flags have been set, the program will perform its defaults operations");
		cFlag = 1;
		lFlag = 1;
		wFlag = 1;
	}
	
	while(optind < argc){
		/*printf("optind: %d",optind);*/
		count(argv[optind],&cFlag,&lFlag,&wFlag,&pFlag,&iFlag,&vFlag);
		results(argv[optind],0);
		optind++;
		fileAmt++;
	}
	
	if(fileAmt > 1){
		results("Total",1);
	}
	
	return(0);	
}


