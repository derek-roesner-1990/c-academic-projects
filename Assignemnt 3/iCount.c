/****************************************************************************************************************************
 * Student - Derek Roesner
 * Student# - 040668227
 * Assignment# - 3
 * C Programming - CST8234
 * Lab Section - 400  
 * Professor - Carolina Ayala  
 * Due - 12:00 Dec 2 2012 
 * Submission Made - Dec 2 2012
 * File(s) Included - iCount.h
 *****************************************************************************************************************************/

#include "iCount.h"

/*The flags do not need to be included as parameters as their values are always available through their definitions in iCount.h 
 They are included as parameters to the function to remind of their importance to the function.*/
void count(char *fileArg,int *cFlag, int *lFlag, int *wFlag, int *pFlag, int *iFlag, int *vFlag){
	int fd;
	int i;
	int matchFound;
	int bytes;
	int bufferIndex;
	int bufferSize = 1024;
	char buffer[bufferSize];
	int lineLength;
	char *line;
	char delimiter[] = " ";
	char * word;
	
	if( (fd = open(fileArg, O_RDONLY)) == -1){
		perror("sourcefile");
		exit(EXIT_FAILURE);
	}
	
	/*If iFlag is set and a pattern is to be matched, the pattern will be modified to ensure 
	 that all characters are lowercase. The pattern will be modified outside of the while loop
	 that splits the buffer into individual words since it only needs to be modified once. 
	 */
	
	if(*pFlag == 1 && *iFlag == 1){
		toLowerCase(pattern);
	}
	
	while( (bytes = read(fd,buffer,bufferSize)) > 0){
		bufferIndex = 0; 
		
		if(*pFlag == 1 || *wFlag == 1 || *lFlag == 1){
			
			while(bufferIndex < bytes){
				/*Increments newLineAmt if \n is found in buffer*/
				lineLength = getLineLength(buffer,'\n',bufferIndex);
				
				/*printf("line length %d\n",lineLength);*/
			
					/*
					If pFlag or wFlag are not set, the line does
					not need to be copied. If this happens the
					value bufferIndex will never be incremented,
					so lineLength is added to bufferIndex to keep 
					track of the current line in the buffer.
					 */
				if(*pFlag == 0 && *wFlag == 0){
					bufferIndex += lineLength;
				}
				
			
				if(*pFlag == 1 || *wFlag == 1){
					line = (char*) malloc(sizeof(char) * lineLength);
					for(i = 0; i < lineLength; i++){
						line[i] = buffer[bufferIndex++];
					}
					
					/*split line into words by finding spaces*/
					for(word = strtok(line,delimiter); word != NULL;  word = strtok(NULL,delimiter)){
						if(*wFlag == 1){
							wordAmt++;
						}
						
						/*printf("word: %s\n",word);*/
				
						if(*pFlag == 1){
							if(*iFlag == 1){
								toLowerCase(word);
							}
					
							/*If the pattern is shorter than the current word, there is a chance of
							 multiple occurances of the pattern appearing within each. Words that
							 are the same length are also compared since comparePattern() will
							 still make a valid comparison, just only once. 
					 
							 If vFlag is set, each word is checked to see if it contains a single
							 occurance of the pattern or not. Multiple occurances don't matter. 
							 */
							if(strlen(pattern) <= strlen(word)){
								matchFound = comparePattern(word,pattern);
								/*printf("matchFound %d",matchFound);*/
								if(*vFlag == 1 && matchFound == 0){
									pAmt++;
								}
							}else{
								/*If the pattern is longer than the current word there is not a chance of
								 making a match, but if the vFlag is set this is what to look for.*/	
								if(*vFlag == 1){
									pAmt++;	
								}
							}
						}
					}
				}
			
				if(buffer[bufferIndex] == '\n'){ 
					bufferIndex++; 
				}
				
			} /* end of while(bufferIndex<bytes) */
		} 
		
		if(*cFlag == 1){
			charAmt += bytes; 
		}
		
	}
	close(fd);
	return;
}

int getLineLength(char * toRead,char delimiter,int offset){
	int lineLength = 0;
	int i;
	
	for(i = offset; toRead[i] != 0; i++){
		
		if(toRead[i] != delimiter){
			lineLength++;
		} else {
			if(lFlag == 1){
				newLineAmt++;
			}
			break;
		}
	}
	return(lineLength);
}

/*
 *toLowerCase() ensures that every character in a string is lowercase 
 *If a character is found to be uppercase (in between the range 65-90)
 *the amount 32 is aded to convert it to a lowercase. See ASCII character chart.
 */
void toLowerCase(char * word){
	int length = strlen(word);
	int j;
	/*printf("before %s",word);*/
	for(j = 0; j < length; j++){
		if(word[j] >= 65 && word[j] <= 90){
			word[j] += 32;
		}
		/*printf("after %s",word);*/
	}
	
	return;
}
/*
 * Return value is used to indicate if a match was found or not. 
   Returns -1 on error. 
 */
int comparePattern(char *word, char *pattern){
	int wordLength = strlen(word);
	int patternLength = strlen(pattern);
	int i;
	int j;
	int matchFound = 0;
	char * toMatch;
	
	if( (toMatch = (char*) malloc(sizeof(char) * patternLength)) == 0){
		return(-1);
	}
	
	for(i = 0; i+patternLength <= wordLength; i++){
		
		for(j = 0; j < patternLength; j++){
			toMatch[j] = word[j+i];
		}
		
		if( strcmp(pattern, toMatch) == 0){
			if(vFlag == 1){
				matchFound = 1;
			} else {
				pAmt++;
			}
		}
		
	}
	return(matchFound);
}

void results(char * resultsFor,int total){
	if(cFlag == 1){ 
		printf("Character/Byte Count: %d ",(total==1) ? cTotal:charAmt);
		cTotal += charAmt; 
		charAmt = 0; 
	} 
	
	if(lFlag == 1){ 
		printf("Newline Count: %d ",(total==1) ? nTotal:newLineAmt); 
		nTotal += newLineAmt; 
		newLineAmt = 0;
	}
	
	if(wFlag == 1){
		printf("Word Count: %d ",(total==1) ? wTotal:wordAmt);
		wTotal += wordAmt;
		wordAmt = 0;
	}
	
	if(pFlag == 1){ 
		printf("Occurances of %s: %d ",pattern,(total==1) ? pTotal:pAmt);
		pTotal += pAmt; 
		pAmt = 0;
	}
	
	printf("-%s\n",resultsFor);
	
	return;
}

void displayUsage(char *helpMessage){
	printf("%s\n", helpMessage); 
	exit(EXIT_FAILURE);
	return;
}




