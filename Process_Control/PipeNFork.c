/*
 *  PipeNFork.c
 *  
 */


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define USAGE "./pipeNFork [FILENAME]"

int main(int argc , char * argv[]){
	int fd[2];
	int file; 
	int bytes;
	char buffer[BUFSIZ];
	char message[BUFSIZ]; /*BUFSUZE defined in stdio.h*/
	
	if(argc != 2){
		printf("Usage %s\n", USAGE);
		exit(EXIT_FAILURE);
	}
		
	if(pipe(fd) == -1){
		perror("Pipe");
		exit(EXIT_FAILURE);
	}
	
	switch(fork()){
		case -1:
			perror("Fork");
			exit(EXIT_FAILURE);
			break;
		case 0:
			close(fd[1]);
			while(read(fd[0],message,BUFSIZ) == -1){
				perror("Read");
				exit(EXIT_FAILURE);
			}
			
			puts(message);
			fflush(stdout);
			break;
		default:
			close(fd[0]);
			
			if(file = open(argv[1],O_RDONLY) == -1){
				perror("Open");
				exit(EXIT_FAILURE);
			}
			
			while( (bytes = read(file,buffer,BUFSIZ)) > 0){
				
				if(write(fd[1],buffer,bytes) == -1){
					perror("Write");
					exit(EXIT_FAILURE);
				}
				
			}
			
			break;
	}
	return(0);
}