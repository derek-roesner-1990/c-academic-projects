/*
 *  ProcessExample.c
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define USAGE "./ProcessExample [message]\n" 

int main(int argc, char * argv[]){
	
	int fd[2];
	char message[BUFSIZ];
	
	/*checks for arguments*/
	if(argc != 2){
		printf("USAGE %s", USAGE);
		exit(EXIT_FAILURE);
	}
	
	/*returns file descriptors for a file
	  1 for input and one for output
	 */
	
	if( (pipe(fd) == -1) ){
		perror("pipe");
		exit(EXIT_FAILURE);
	}
	
	switch(fork()){
	case -1:
		perror("fork");
		break;
	case 0:
		//child
		close(fd[1]);//close outputstream
		if( read(fd[0],message,BUFSIZ) == -1){
			perror("Read");
			exit(EXIT_FAILURE);
		}
		puts(message);
		break;
	default://any other pid is the parent
		close(fd[0]);
		if( write(fd[1],argv[1],BUFSIZ) == -1){
			perror("Write");
			exit(EXIT_FAILURE);
		}
		break;
	}
  return 0;
}	   
	
	
	
	//create the pipe
	//fork the parent process
	//determine which pid is current
	//if child - close the output stream
	//         - read input until no more is needed
	//if parent - close the input stream
	//          - write into the parent
	
	//flush stdout() fflush() why???
	
	//this is possible because both id's actualy contain the same streams. 



