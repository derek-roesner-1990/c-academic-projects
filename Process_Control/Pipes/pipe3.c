/* Using a pipe to send data from a parent to a child process
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[ ]) {
  	int            f_des[2];
	static char    message[BUFSIZ];

  	if (argc != 2) {
    		printf("Usage:  %s message\n",*argv);
    		return 1;
  	}

  	if (pipe(f_des) == -1) {             // generate the pipe
    		perror("Pipe");     
		return 2;
  	}

  	switch (fork( )) {
  	case -1:
    		perror("Fork");     
		return 3;
  	case 0:                              // In the child
    		close(f_des[1]); //f_des[0] for reading, f_des[1] for writing.
    		if (read(f_des[0], message, BUFSIZ) != -1) {
      			printf( "Message received by child: [ %s ]\n", message );
      			fflush(stdout);
    		} else {
      			perror("Read");    
			return 4;
   		}
    		break;
  	default:                             // In the Parent
    		close(f_des[0]);
    		if (write(f_des[1], argv[1], strlen(argv[1])) != -1) {
			printf( "Message sent by parent: [ %s ]\n", argv[1] );
      			fflush(stdout);
    		} else {
      			perror("Write");   
			return 5;
    		}
  	}
  	return 0;
}

