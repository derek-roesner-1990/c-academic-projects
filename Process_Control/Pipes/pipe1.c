/*
** pipe1.c -- a dumb pipe example
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>


int main(void)
{
	int pfds[2];
	char buf[30];
	char string[30];

	if (pipe(pfds) == -1) {
		perror("pipe");
		exit(1);
	}

	printf("writing to file descriptor #%d\n", pfds[1]);
	printf("Enter string to write to pipe: ", string );
	scanf("%s", string );
	write(pfds[1], string, strlen( string ));
	printf("reading from file descriptor #%d\n", pfds[0]);
	read(pfds[0], buf, strlen( string ));
	printf("read \"%s\"\n", buf);

	return 0;
}

