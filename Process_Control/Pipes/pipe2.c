 /*
** pipe2.c -- a smarter pipe example
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int main(void)
{
	int pfds[2];
	char buf[30];
	char string[30];

	pipe(pfds);

	if (!fork()) {
		printf(" CHILD: writing to the pipe\n");
		printf("Enter string to write to pipe: ", string );
		scanf("%s", string );
		write(pfds[1], string, 30);;
		printf(" CHILD: exiting\n");
		exit(0);
	} else {
		printf("PARENT: reading from pipe\n");
		read(pfds[0], buf, 30);
		printf("PARENT: read \"%s\"\n", buf);
		wait(NULL);
	}

	return 0;
}

