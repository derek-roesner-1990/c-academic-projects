/*
** fork1.c -- demonstrates usage of fork() and wait()
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

int main(void)
{
	pid_t pid;
	int rv;
	int fd;
	char p, c;

	if ( (fd = open("data.txt", O_RDWR )) == - 1 ) {
		perror("open");
		exit( EXIT_FAILURE );
	}

	switch(pid = fork()) {
	case -1:
		perror("fork");  /* something went wrong */
		exit( EXIT_FAILURE );

	case 0:
		printf(" CHILD: This is the child process!\n");
		printf(" CHILD: My PID is %d\n", getpid());
		printf(" CHILD: My parent's PID is %d\n", getppid());
		read( fd, &c, 1 );
		printf(" CHILD: c = %c\n", c );
		printf(" CHILD:  Enter my exit status: ");
		scanf(" %d", &rv);
		printf(" CHILD: I'm outta here!\n");
		exit(rv);

	default:
		printf("PARENT: This is the parent process!\n");
		printf("PARENT: My PID is %d\n", getpid());
		printf("PARENT: My child's PID is %d\n", pid);
		read( fd, &p, 1 );
		printf("PARENT: p = %c\n", p );
		printf("PARENT: I'm now waiting for my child to exit()...\n");
		wait(&rv);
		printf("PARENT: My child's exit status is: %d\n", WEXITSTATUS(rv));
		printf("PARENT: I'm outta here!\n");
	}

	return 0;
}

