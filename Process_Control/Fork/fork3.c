/*
** fork1.c -- demonstrates usage of fork() and wait()
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(void)
{
	pid_t pid;
	int rv;
	int i;

	switch(pid = fork()) {
	case -1:
		perror("fork");  /* something went wrong */
		exit(1);		 /* parent exits */

	case 0:
		printf(" CHILD: This is the child process!\n");
		for( i = 0; i < 20; i++ ) {
			printf(" CHILD: %d\n", i);
			sleep( 1 );
		}
		printf(" CHILD: I'm outta here!\n");
		exit(0);

	default:
		printf("PARENT: This is the parent process!\n");
		for( i = 0; i < 20; i += 3 ) {
			printf("PARENT: %d\n", i);
			sleep( 1 );
		}
		printf("PARENT: I'm now waiting for my child to exit()...\n");
		wait(&rv);
		printf("PARENT: My child's exit status is: %d\n", WEXITSTATUS(rv));
		printf("PARENT: I'm outta here!\n");
	}

	return 0;
}

