/* PROGRAM:  command.c
   AUTHOR:   Robbins & Robbins
	     Mod. by Carolina Ayala
   DATE:     02/Feb/02
	     29/Nov/11
   PURPOSE:  To demostrate the use of fork and exec combinate
	     To create a very simple shell
   NOTES:
	     The exevp( ) system call needs an array of vectors terminated
             in NULL ( just as argv )

	     execvp:
           
             int execvp( const char *file, char *const argv[] );

	     where:  file is an executable, does not need to include the path
                     as execvp will seach in the PATH variable
                       
                     argv is the an array of pointers to null-terminates strings
                     representing the argument list avaialbe to the program ( file )

*/

/**************************************************************************/
/* Include necessary header files
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>

/**************************************************************************/
/* Main function 
 **************************************************************************/
int main ( int argc, char *argv[] ) {

	pid_t pid ;

	if ( ( pid = fork() ) == -1 ) {
		perror("Fork");
		exit( EXIT_FAILURE );
	}
	else if( pid == 0 ) {
		printf(" CHILD: This is the child process!\n\n");
		execvp( argv[1], &argv[1] );
		/*
		 *  No need for an if, 
		 *  if it gets here it was an error!
		 */
		perror("Exec");
		exit( EXIT_FAILURE );
   	}
	else {
		printf("PARENT: This is the parent process!\n\n");
		/*
	     	 * Don't care about the return value of the child
		 */
		wait( NULL );
		printf("\nPARENT: The CHILD executed %s\n", argv[1]);
	}

	return 0;
}
